﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="MAM.SearchResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>상세 검색</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=1, minimum-scale=0.5, user-scalable=yes, target-desitydpi=device-dpi" />
    <script type="text/javascript" src="contents/js/html5shiv.js"></script>
	<link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" />
<%--    <link rel="stylesheet" type="text/css" href="SearchResult.css" />--%>
    <link href="contents/css/reset.css" rel="stylesheet" type="text/css" />
    <% int age = Request.Cookies["BIRTHYEAR"]==null?-1:(Int32.Parse((System.Web.HttpUtility.UrlDecode(Request.Cookies["BIRTHYEAR"].Value)==""?"-1":System.Web.HttpUtility.UrlDecode(Request.Cookies["BIRTHYEAR"].Value)))); %>
    <% int bs = Request.Cookies["BS"]==null?-1:Int32.Parse(System.Web.HttpUtility.UrlDecode(Request.Cookies["BS"].Value)); %>
    <% if(age>=50 && bs !=-1 && bs < 15) { %>
            <link href="contents/css/common50.css" rel="stylesheet" />
    <% } else if (age >= 50 && (bs >= 15 || bs == -1))       { %>
            <link href="contents/css/common50.css" rel="stylesheet" />
    <% } else if (age<50 && bs <15) { %>
        <link href="contents/css/common.css" rel="stylesheet" />
    <% } else { %>
        <link href="contents/css/common.css" rel="stylesheet" />
    <% } %>

</head>
<body>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data">
        <header id="header">
    		<h1>
		    	<a href="SearchResult.aspx?wa=<%=WA%>"><img src="img/common_logo.png" alt="MAM 검색 서비스" /></a>
	    	</h1>
		    <div class="search_wrap">
			    <div>
                    <asp:TextBox ID="SearchTextbox2" class="searchtextbox" runat="server" OnTextChanged="SearchTextbox2_TextChanged" ></asp:TextBox>
                    <asp:Button ID="ImageButton1" CssClass="btn_search" runat="server" OnClick="SearchCommand2_Click"   />
		            <div class="search_btn_wrap">
			            <div class="search_btn2">
                        <img id="fUpload3" width="30" height="20"  onclick="browseVideo();" src="img/main_bg_videosearch.png" alt="" />

			            </div>
		            </div>

			    </div>
		    </div>

                <div style="display: none">
                            <asp:FileUpload ID="fUpload" runat="server" onchange="UploadFileNow()" AllowMultiple="false"  style="display: none" accept="*/*;capture=camera" /> 
                            <asp:Button ID="btnUpload" Text="Uploads" runat="server" OnClick="btnUpload_Click" />
                </div>

        </header>

        <div id="wrap">
            <aside id="gnb">

            </aside>
            <div id="contents">
                <% if ((!string.IsNullOrEmpty(Keywords) || !string.IsNullOrEmpty(VideoSearch)) ) { %>
                    <% if(vSearchResults2 != null) { %>
                    <section id="video_result">
                        <header>						    <h2>동영상 검색 결과</h2>
						    <span><%=vSearchResults2.Count() %></span>
					    </header>
                        <div class="container">
                            <ul class="video_list">
                            <% int vcount = 0; %>
                            <% int vmax = (vCnt <= vSearchResults2.Count() ? vCnt : vSearchResults2.Count()); %>
                            <% if (vSearchResults2.Count() < 4) vmax = vSearchResults2.Count(); %>
                            <% foreach(var v2 in vSearchResults2.OrderBy(v=>v.RGBdifferce)) { %>
                                <li>
                                    <div class="thumbnail">
                                            <video id="myVideo<%=vcount.ToString() %>"  width="235px"  poster="http://221.143.42.141:3000/Storage/pictures//<%=v2.imgname %>"  controls >
<%--                                                <source src="contents/video/[PDgreatspirit]gullezzak.mp4" type="video/mp4" />--%>
                                                <source src="http://221.143.42.141:3000/Storage/videos/<%=v2.filename %>" type="video/mp4" />
                                                Your browser does not support HTML5 video.
                                            </video>    
                                    </div>
                                    <%  if (Request.Cookies["PHONE"] == null || Request.Cookies["PHONE"].Value != "PHONE") { %>
                                        <div class="video_quality normal"> </div>
                                    <%} else { %>
                                        <div class="video_quality"> </div>    
                                    <% } %>
                                    <% if (WA=="on") { %>
                                        <%  if (Request.Cookies["BS"] == null || Int32.Parse(Request.Cookies["BS"].Value) >= 15) { %>
                                            <div class="play_wrap" onclick="play1('<%=/*(!Request.UserAgent.Contains("T700")&&Request.UserAgent.Contains("Android"))?v2.filename360:*/v2.filename%>');" ></div>
                                        <% } else { %>
                                            <div class="play_wrap unable"></div>
                                        <% } %>
                                    <% } else { %>
                                    
                                            <div class="play_wrap" onclick="playFullScreen('myVideo<%=vcount.ToString() %>');" ></div>
                                    <% } %>
                                </li>
                            <%      if (++vcount >= vmax) break;
                                } %>
                            </ul>
					    </div>

                        <% if(vSearchResults2.Count() > vmax) { %>
        			    <div class="btn_wrap">
					        <a href="SearchResult.aspx?wa=<%=WA%>&keywords=<%=Keywords%><%=vCnt>=vSearchResults2.Count()?"&vCnt="+vCnt.ToString():"&vCnt="+(vCnt+DefaultVideos).ToString()%>#<%="V"+(vcount).ToString()%>" class="btn_more">
                                <span>비디오 검색 결과 더보기</span>
					        </a>
				        </div>
                        <% } %>
                    </section>
                    <% } %>
                <% } %>

                <div ><img id="anigif" src="img/animated.gif" style="position:absolute; display: none; z-index: 10; left:350px; top:400px;" alt="" /></div>

			</div>
        </div>
        <script type="text/javascript" src="contents/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="contents/js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="contents/js/default.js"></script>
        <script type="text/javascript">
            <!--
    function popup(mylink, windowname, titlename) {
        if (!window.focus) return true;
        var href;
        if (typeof (mylink) == 'string')
            href = mylink;
        else
            href = mylink.href;
        var w = window.open(href, windowname, "width=800,height=800,scrollbars=yes,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,resizable=yes,fullscreen=yes");
        w.document.title = titlename;
        w.focus();
        return false;
    }
        </script>
        <script type="text/javascript">
            function browse() {
                <% if(Request.UserAgent.Contains("Android")) { %>
                window.app.showImagePicker();
                <% } %>
                document.getElementById('<%= fUpload.ClientID %>').click();
            }
            function browseVideo() {
                <% if(Request.UserAgent.Contains("Android")) { %>
                window.app.showVideoPicker();
                <% } %>
                document.getElementById('<%= fUpload.ClientID %>').click();
            }
            function play1(videoname) {
                <% if(Request.UserAgent.Contains("Android")) { %>
                window.app.play1(videoname);
                <% } %>


            }
            function playFullScreen(elementid) {
                element = document.getElementById(elementid);
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
                element.load();
                element.play();
//                element.webkitEnterFullscreen();
            }
        </script>
        <script type="text/javascript">
            function DisableControl(controlId) {
                //document.getElementById(controlId).disabled = true;
                document.getElementById(controlId).style.visibility = 'hidden';
            }
            function DisableControl_SetTimeout(controlid, interval) {
                setTimeout("DisableControl('" + controlid + "')", interval);
            }
            function UploadFileNow() {
                ag = document.getElementById('anigif');
                ag.style.left = (window.innerWidth / 2 - 50) + 'px';
                ag.style.top = (window.innerHeight / 2 - 50) + 'px';
                $("#anigif").show();
                ag.style.visibility = 'visible';
                DisableControl_SetTimeout('anigif', 20000);
                var value = $("#fUpload").val();
                if (value != '') {
                    $("#form1").submit();
                }
            }

            //-->
        </script>
        <!--[if lt IE 9]>
        <script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

    </form>


</body>
</html>
