﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchMain.aspx.cs" Inherits="MAM.SearchMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=1, minimum-scale=0.5, user-scalable=yes, target-desitydpi=device-dpi" />
    <title>메인 검색</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" />
    <link rel="stylesheet" type="text/css" href="Search.css" />
</head>
<body>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data" defaultbutton="SearchCommand">
        <header id="header">
		    <div class="login_wrap">
 
		    </div>
        </header>       
        <%-- <div style="overflow: hidden;">--%>
            <div id="mainLogo">
                <asp:Image ID="MainLogo" runat="server" class="mamlogo" ImageUrl="~/img/common_logo.png" alt="MAM 검색 서비스" ImageAlign="Middle" />
            </div>
       
        <div class="search_wrap">
			<div>
                <asp:TextBox ID="SearchTextbox" class="searchtextbox" runat="server" AutoCompleteType="None" TextMode="SingleLine"></asp:TextBox>
              <asp:ImageButton ID="SearchCommand" CssClass="btn_search" runat="server" OnClick="SearchCommand_Click" ImageUrl="~/img/imgsearch_btn_search.png"   /> 
		        <div class="search_btn_wrap">
			        <div class="search_btn1">
                        <img id="fUpload3" width="30" height="20"   onclick="browseVideo();" src="img/main_bg_videosearch.png" alt="" />
			        </div> 
			        <div class="search_btn2"></div>
		        </div>
			</div>
		</div>
        <div style="display: none">
                    <asp:FileUpload ID="fUpload" runat="server" onchange="UploadFileNow()" AllowMultiple="false"  style="display: none" accept="*/*;capture=camera" /> 
                    <asp:Button ID="btnUpload" Text="Uploads" runat="server" OnClick="btnUpload_Click"  style="display: none"/>

        </div>

        <div ><img id="anigif" src="img/animated.gif" style="position:absolute; display: none; z-index: 10" alt="" /></div>

   <table height="70px" border="0" />
        <table align="center" border="0" >
            <tr>
  
                <td><asp:ImageButton ID="VideoSearch" runat="server" CssClass="btn_search" ImageUrl="img/main_bg_videosearch.png" OnClick="VideoSearch_Click" ImageAlign="Middle" /></td>
                <td width="18px"></td>
                <td align="left"><strong>동영상 검색</strong><br />동영상을 통해 검색할<br />수 있습니다.</td>
            </tr>
        </table>
        <table align="center" border="0" >
            <tr height="50px">
                <td></td>
            </tr>
            <tr>
                <td align="center"><div id="KETI_Banner" style="height:100px; width:500px;"></div></td>
            </tr>
        </table>


        <script type="text/javascript" src="contents/js/jquery-1.11.1.min.js"></script>
        <script src="http://r-eum.com/web/js/keti.js" language="javascript" type="text/javascript"></script>
        <script type="text/javascript" >
            $(document).bind("mobileinit", function () {
                $.support.cors = true;
                $.mobile.allowCrossDomainPages = true;
            })(jQuery);
        </script>
        <script language="javascript" type="text/javascript">
            function browse() {
                <%--                document.getElementById('<%= fUpload.ClientID %>').click();--%>
                //window.jsi.showPicker();
                <% if(Request.UserAgent.Contains("Android")) { %>
                    window.app.showImagePicker();
                <% } %>
                $("#fUpload").click();
            }
            function browseVideo() {
                <% if(Request.UserAgent.Contains("Android")) { %>
                window.app.showVideoPicker();
                <% } %>
                document.getElementById('<%= fUpload.ClientID %>').click();
            }

            //-->
        </script>
        <script type="text/javascript">
            function DisableControl(controlId) {
                //document.getElementById(controlId).disabled = true;
                document.getElementById(controlId).style.visibility = "hidden";
            }
            function DisableControl_SetTimeout(controlid, interval) {
                setTimeout("DisableControl('" + controlid + "')", interval);
            }
            function UploadFileNow() {
                ag = document.getElementById('anigif');
                ag.style.left = (window.innerWidth / 2 - 50) + 'px';
                ag.style.top = (window.innerHeight / 2 - 50) + 'px';
                $("#anigif").show();
                ag.style.visibility = 'visible';
                DisableControl_SetTimeout('anigif', 20000);
                var value = $("#fUpload").val();
                if (value != '') {
                    $("#form1").submit();
                }
            }
          
        </script>

    </form>
</body>
</html>
