﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="MAM.SearchResult" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MAM</title>
    <meta charset="UTF-8" />
    <script type="text/javascript" src="contents/js/html5shiv.js"></script>
	<link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" />
<%--    <link rel="stylesheet" type="text/css" href="SearchResult.css" />--%>
    <link href="contents/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="contents/css/common.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data">
        <header id="header">
    		<h1>
		    	<img src="img/common_logo.png" alt="MAM 검색 서비스" />
	    	</h1>
<%--            <asp:Image ID="TopLogo" runat="server" class="toplogo" ImageUrl="~/img/common_logo.png" ImageAlign="Left" />--%>
		    <div class="search_wrap">
			    <div>
<%--				    <input type="text" />--%>
<%--    			    <a href="#" class="btn_search"><span class="blind">검색</span></a>--%>
                    <asp:TextBox ID="SearchTextbox2" class="searchtextbox" runat="server" ></asp:TextBox>
                    <asp:ImageButton ID="ImageButton1" CssClass="btn_search" runat="server" OnClick="SearchCommand_Click"   />
			    </div>
		    </div>
<%--            <asp:TextBox ID="SearchTextbox" class="searchtextbox" runat="server" ></asp:TextBox>
            <asp:ImageButton ID="SearchCommand" CssClass="searchcommand" runat="server" ImageUrl="img/common_btn_search.png" OnClick="SearchCommand_Click"   />--%>
                <% if (searchOption == "i") { %>
                    <asp:FileUpload ID="fUpload" runat="server" />
                    <asp:Button ID="btnUpload" Text="Uploads" runat="server" OnClick="btnUpload_Click" />
                    <asp:Image ID="upFile" runat="server"  width="40px" height="40px" src="img/question-mark.png" />
                <% } %>  
		    <div class="login_wrap">
			    <ul>
				    <li><a href="#">로그아웃</a></li>
				    <li><a href="#">사용자</a>님</li>
				    <li class="header_profile"><a href="#"><img src="img/temporary_img_profile.png" alt="" /></a></li>
			    </ul>
		    </div>
        </header>
        <div id="wrap">
            <aside id="gnb">
    			<ul>
				    <li class="<%=(searchOption=="a"?"on":"")%>"><a href="SearchResult.aspx?sop=a&ti=0&keywords=<%=Keywords%>">통합 검색</a></li>
				    <li class="<%=(searchOption=="i"?"on":"")%>"><a href="SearchResult.aspx?sop=i&ti=0&keywords=<%=Keywords%>">이미지 검색</a></li>
				    <li class="<%=(searchOption=="t"?"on":"")%>"><a href="SearchResult.aspx?sop=t&ti=0&keywords=<%=Keywords%>">텍스트 검색</a></li>
				    <li class="<%=(searchOption=="v"?"on":"")%>"><a href="SearchResult.aspx?sop=v&ti=0&keywords=<%=Keywords%>">동영상 검색</a></li>
			    </ul>
<%--                <div id="LEFT1" class="left1" >
                    <asp:Button ID="TongHapButton" class="leftbutton" runat="server" Text="통합 검색" style="background-color: Transparent" OnClick="TongHapButton_Click"/>
                </div>
                <div id="LEFT2" class="left2" >
                    <asp:Button ID="Button1" class="leftbutton" ForeColor="#000000" runat="server" Text="이미지 검색" style="background-color: Transparent" OnClick="TongHapButton_Click"/>
                </div>
                <div id="LEFT3" class="left2" >
                    <asp:Button ID="Button2" class="leftbutton" ForeColor="#000000" runat="server" Text="텍스트 검색" style="background-color: Transparent" OnClick="TongHapButton_Click"/>
                </div>
                <div id="LEFT4" class="left2" >
                    <asp:Button ID="Button3" class="leftbutton" ForeColor="#000000" runat="server" Text="동영상 검색" style="background-color: Transparent" OnClick="TongHapButton_Click"/>
                </div>--%>
            </aside>
            <div id="contents">
                <% if ((Keywords != null && Keywords != "") && (searchOption == "a" || searchOption == "v")) { %>
                    <section id="video_search">
                        <header>
						    <h2><a href="#">동영상 검색 결과</a></h2>
						    <span>002</span>
					    </header>
                        <div class="container">
                            <a href="videoplayer.aspx?vname=http://221.143.42.141:8088/media/[720]/[720p][AJU TV] 금주의 지역축제 TOP5 - 진해군항제 편.mp4" onclick="return popup(this, 'notes', '진해 군항제')" title="진해 군항제">
                                <video  width="200px"  >
                                    <source src="http://221.143.42.141:8088/media/[720]/[720p][AJU TV] 금주의 지역축제 TOP5 - 진해군항제 편.mp4" type="video/mp4" />           
                                    <span class="play_wrap">재생하기</span>
                                </video>
                            </a>
                            <a href="videoplayer.aspx?vname=http://221.143.42.141:8088/media/[720]/[720p][HIT] VJ 특공대-국내 최초 '비키니 해변' 강원도 강릉 사근진!.20140725.mp4" onclick="return popup(this, 'notes','강릉 사근진')" title="강릉 사근진">
                                <video width="200px" >
                                    <source src="http://221.143.42.141:8088/media/[720]/[720p][HIT] VJ 특공대-국내 최초 '비키니 해변' 강원도 강릉 사근진!.20140725.mp4" type="video/mp4" />           
                                </video>
                            </a>
					    </div>
        			    <div class="btn_wrap">
					        <a href="#" class="btn_more"><span>검색 결과 더보기</span></a>
				        </div>
                    </section>
                <% } %>

                <%if (ibt != null && (searchOption=="a" || searchOption=="i")) { %>
				    <section id="img_search">
					    <header>
						    <h2>이미지 검색 결과</h2>
						    <span><%= (ibt!=null?ibt.ListCount:0) %></span>
					    </header>
					    <div class="container">
    					    <div class="img_wrap">
						        <ul id="container">
                                    <% foreach (var img1 in ibt.SearchResult) { %>
                                        <li class="item">
                                            <a href="<%=img1.ImgOrgURL%>" onclick="return popup(this, 'notes', '<%=img1.ImgTag %>')" title="<%=img1.ImgTag %>">
                                                <img src="<%=img1.ImgThumbURL%>" alt="<%=img1.ContentTitle %>" />
                                            </a> </li>
                                    <% } %>
						        </ul>
                            </div>
					    </div>
    <%--    				
                        <div class="relate">
						    <div class="relate_wrap">
							    <h3>유사이미지 재검색</h3>
							    <ul >
								    <li><a href="#"><img src="contents/img/thumbnail1.png" alt="" /></a></li>
								    <li><a href="#"><img src="contents/img/thumbnail2.png" alt="" /></a></li>
								    <li><a href="#"><img src="contents/img/thumbnail3.png" alt="" /></a></li>
								    <li><a href="#"><img src="contents/img/thumbnail4.png" alt="" /></a></li>
								    <li><a href="#"><img src="contents/img/thumbnail5.png" alt="" /></a></li>
							    </ul>
						    </div>
					    </div>
        --%>

        			    <div class="btn_wrap">
					        <a href="#" class="btn_more"><span>검색 결과 더보기</span></a>
				        </div>
				    </section>
                <% } %>

                <%if (ts != null && (searchOption == "a" || searchOption == "t"))
                  { %>
                    <section class="txt_search">
					    <header>
						    <h2>텍스트 검색 결과</h2>
						    <span><%= (ts!=null?ts.ListCount:0) %></span>
                        </header>
                        <div class="container">
    <%--                        inputKeywords: <%= ts.inputKeywords %> <br />
                            interpretation: <%= ts.interpretation %> <br />
                            totalContentCount: <%= ts.totalContentCount %> <br />--%>
                            <div>
					            <ul class="lst_type">
                                <% int cnt = 0; %>
                                <% foreach (var tc in ts.SearchResult) { %>
                                    <li>
                                        <a href="ViewContents.aspx?contentId=<%=tc.contentID%>" onclick="return popup(this, 'notes', '<%= tc.contentName %>')" ><img src="img/question-mark.png" width="90" height="90" alt="" /></a>
                                        <%= (cnt == 0) ? "" : "<br />"  %>
                                        <a href="ViewContents.aspx?contentId=<%=tc.contentID%>" onclick="return popup(this, 'notes', '<%= tc.contentName %>')" >
                                            <strong><%= tc.contentName %></strong>
                                            <%= "   " + tc.address %>
                                            <br />
                                            <%= (tc.overview.Length > 200) ? tc.overview.Substring(0, 200) + "..." : tc.overview %> <br />
                                            <%= (tc.telephone == null) ? "전화: " + tc.telephone : "" %>
                                        </a>
                                        <% if (cnt++ > 10) break; %>                                
                                    </li>
                                <% } %>
                                </ul>
                            </div>
                        </div>

                        <%if (ti != null) { %>
                           <div class="lst_intention_wrap">
                                <header>
						            <h2>텍스트 의도 재검색</h2>
						            <span><%= (ti!=null?ti.Num_TagList:0) %></span>
                                </header>
                            </div>
 					        <ul class="lst_intention">
                            <%int cntTI = 0; %>
                            <% foreach (var iinfo in ti.TagList) { %>
                                <li>
                                    <a href="SearchResult.aspx?sop=<%=searchOption %>&ti=<%=cntTI %>&keywords=<%=Keywords %>" >
                                        <%=(cntTI == txtIntension) ? ("<strong>" + iinfo.TextTagName + "</strong>") : iinfo.TextTagName %>
                                        <%cntTI++; %>
                                    </a>
                                </li>
                            <% } %>
                            </ul>
                        <% } %>

                        </section>
                    <% } %>
			</div>
        </div>
        <script type="text/javascript" src="contents/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="contents/js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="contents/js/default.js"></script>
        <script type="text/javascript">
            <!--
            function popup(mylink, windowname, titlename) {
                if (!window.focus) return true;
                window.close()
                var href;
                if (typeof (mylink) == 'string')
                    href = mylink;
                else
                    href = mylink.href;
                var w = window.open(href, windowname, "width=800,height=600,scrollbars=yes,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,resizable=no");
                w.document.title = titlename;
                w.focus();
                return false;
            }
            //-->
        </script>
        <!--[if lt IE 9]>
        <script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

    </form>


</body>
</html>
