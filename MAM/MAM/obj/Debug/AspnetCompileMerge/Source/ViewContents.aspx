﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewContents.aspx.cs" Inherits="MAM.ViewContents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <header>
			<h2><%=C_Title %></h2>
		</header>
        <table>
            <tr>
                <td><img src="<%=C_Pic1%>" /></td>
                <td><img src="<%=C_Pic2%>" /></td>
            </tr>
        </table>
        <table>
            <tr>
                <td width="200px" align="left" bold="true">위 치</td>
                <td><%=C_Zipcode + " " + C_Addr1 + " " + C_Addr2 %></td>
            </tr>
            <tr>
                <td width="200px" align="left" bold="true">상세 요약</td>
                <td><%=C_Overview %></td>
            </tr>
            <tr>
                <td width="200px" align="left" bold="true">인포메이션</td>
                <td><%=C_Info %></td>
            </tr>
            <tr>
                <td width="200px" align="left" bold="true">이용 시간</td>
                <td><%=C_UseTime %></td>
            </tr>
            <tr>
                <td width="200px" align="left" bold="true">이용 요금</td>
                <td><%=C_UseFee %></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
