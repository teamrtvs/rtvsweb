﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchMain.aspx.cs" Inherits="MAM.SearchMain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="Search.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="overflow: hidden;">
            <div id="mainLogo">
                <asp:Image ID="MainLogo" runat="server" class="mamlogo" ImageUrl="~/img/main_logo.png" ImageAlign="Middle" />
            </div>
            <div id="mainText" class="maintext" >
                <asp:Label ID="lblMainText" class="maintextbox" runat="server">콘텐츠의 수집, 특성정보 추출/분석 및 부가정보 확장을 통해 해당 데이터를
                    <br />자산화하고, 사용자 상황정보의 분석 및 질의의도에 대한 추론을 기반으로 소비자가
                    <br />원하는 멀티미디어 콘텐츠를 보다 정확하고 쉽게 접근할 수 있도록 하는 검색 서비스
                </asp:Label>
            </div>
        </div>
<%--        <div id="SearchBox" class="searchbox">
            <asp:TextBox ID="SearchTextbox" class="searchtextbox" runat="server"></asp:TextBox>
            <asp:ImageButton ID="SearchCommand" CssClass="searchcommand" runat="server" ImageUrl="~/img/common_btn_search.png" OnClick="SearchCommand_Click"  />
        </div>--%>
        <div class="search_wrap">
			<div>
                <asp:TextBox ID="SearchTextbox" class="searchtextbox" runat="server" ></asp:TextBox>
                <asp:ImageButton ID="SearchCommand" CssClass="btn_search" runat="server" OnClick="SearchCommand_Click" ImageUrl="~/img/imgsearch_btn_search.png"   />
			</div>
		</div>
        <div class="fixedtest">
                Here is my content!
        </div>

        <div style="overflow: hidden;">
            <div id="imgLogo">
                <asp:ImageButton ID="ImageSearch" runat="server" CssClass="btn_search" ImageUrl="img/main_bg_imgsearch.png" OnClick="ImageSearch_Click" ImageAlign="Middle" />
            </div>
        </div>
    </form>
</body>
</html>
