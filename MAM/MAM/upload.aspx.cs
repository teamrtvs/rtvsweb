﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MAM
{
    public partial class upload : System.Web.UI.Page
    {
        void Page_Load(object sender, EventArgs e)
        {
            string filename = Request.QueryString["uName"].ToString();
            //StreamWriter file1 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
            //file1.WriteLine(ID);
            //file1.Close(); //디버깅 계속................

            foreach (string f in Request.Files.AllKeys)
            {
                //ASCIIEncoding encoder = new ASCIIEncoding();

                //StreamWriter file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                //file2.WriteLine("ewrerwerwr : "+f);
                //file2.Close(); //디버깅 계속................
        
               HttpPostedFile file = Request.Files[f];
                string extension = Path.GetExtension(file.FileName);
                

                if (string.Equals(".jpeg", extension, StringComparison.OrdinalIgnoreCase) == true  ||
                 string.Equals(".png", extension, StringComparison.OrdinalIgnoreCase) == true||
                 string.Equals(".bmp", extension, StringComparison.OrdinalIgnoreCase) == true ||
                 string.Equals(".gif", extension, StringComparison.OrdinalIgnoreCase) == true ||
                 string.Equals(".jpg", extension, StringComparison.OrdinalIgnoreCase) == true)
                {
                    file.SaveAs("C:\\RTVS\\Resources\\Storage\\pictures\\" + filename );
                }
                else if (string.Equals(".mp4", extension, StringComparison.OrdinalIgnoreCase) == true||
                string.Equals(".H264", extension, StringComparison.OrdinalIgnoreCase) == true)
                {
                    file.SaveAs("C:\\RTVS\\Resources\\Storage\\videos\\" + filename );
                }
                
                else { }

            }
        }
    }
}