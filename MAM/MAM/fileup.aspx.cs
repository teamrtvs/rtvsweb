﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MAM
{
    public partial class fileup : System.Web.UI.Page
    {

        public string WA = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            // 업로드할 파일 정의를 가지고 온다. uploadedfile은 안드로이드 소스 지정한 레이블임으로 반드시 동일해야함
            HttpPostedFile fp = Request.Files["uploadedfile"];
 
            // 저장할 폴더 - 웹서버의 Upload 디렉토리에 파일 저장(파일 업로드시에는 서버의 절대 경로가 반드시 필요함)

            string file_name = fp.FileName;
            string targetDirectory  = "C:\\RTVS\\Resources\\VideosUploaded\\"+file_name;
 
            try
            {
                fp.SaveAs(targetDirectory);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return;
            }
            // 업로드 성공
            Response.Write("Uploaded =" + targetDirectory);
        

        
        }
    }
}