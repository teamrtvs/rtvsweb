﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace MAM.Models
{
    public class Utils
    {
        //-------------------------------------------------------------------------------------
        /** @brief: 쿠키 매니저 함수
            @param cid: 쿠키 아이디
            @param cname: 쿠키 값
            @return 없음
            @remark 쿠키값이 존재하면 값을 업데이트하고 없으면 새롭게 만들어서 값을 지정한다.
         */
        //----------------------------------------------------------------------------------
        public static void CookieManager(string cid, string cname)
        {
            HttpCookie myCookie = HttpContext.Current.Request.Cookies[cid];
            cname = System.Web.HttpUtility.UrlEncode(cname);
            if (myCookie == null)
            {
                myCookie = new HttpCookie(cid, cname);
                myCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
            if (!myCookie.Value.Equals(cname))
            {
                HttpContext.Current.Response.Cookies.Remove(cid);
                myCookie = new HttpCookie(cid, cname);
                myCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
        }

        public static string getUniqueID()
        {
            return Guid.NewGuid().ToString("N");                
        }

    }
}