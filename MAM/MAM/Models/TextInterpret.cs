﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MAM.Models
{
    /// <summary>
    /// 질의 의도 검색 결과
    /// </summary>
    [Serializable]
    public class TextInterpret
    {
        /// 유저가 입력한 텍스트
        public string inputText { get; set; }
        /// 태그 목록에 있는 갯수
        public int Num_TagList { get; set; }
        /// 태그 목록
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TagInfoList> TagList { get; set; }
    }

    /// <summary>
    /// 태그 정보 목록
    /// </summary>
    public class TagInfoList
    {
        /// 텍스트 태그명
        public string TextTagName { get; set; }
        /// 의도 분석 결과
        public string interpretation { get; set; }  
    }
}