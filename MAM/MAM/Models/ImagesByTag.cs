﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Security.Permissions;

namespace MAM.Models
{
    /// 태그에 의한 이미지 검색 결과를 보관하는 객체
    [Serializable]
    public class ImagesByTag
    {
        /// 현재 페이지
        public int CurrenPageNo { get; set; }
        /// 전체 페이지
        public int TotalPages { get; set; }
        /// 전체 총 갯수
        public int ListCount { get; set; }

        /// 현재 검색 결과
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SearchResult> SearchResult { get; set; }
    }

    /// 검색 결과 목록
    [HostProtectionAttribute(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
    public class SearchResult 
    {
        /// 이미지 태그
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ImgTag { get; set; }
        /// 콘텐츠 제목
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ContentTitle { get; set; }
        /// 썸네일 URL
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ImgThumbURL { get; set; }
        /// 원본 URL
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ImgOrgURL { get; set; }
    }
}