﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Security.Permissions;

namespace MAM.Models
{
    /// <summary>
    ///  비디오 검색 결과
    /// </summary>
    public class VSearchResult
    {
        /// 비디오 아이디
        public int VideoID { get; set; }
        /// 프레임 인덱스
        public int idx { get; set; }
        /// RGB 차이
        public int RGBdifferce { get; set; }
    }
}