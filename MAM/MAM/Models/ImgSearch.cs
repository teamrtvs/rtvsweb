﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Security.Permissions;

namespace MAM.Models
{
    /// 이미지 검색 결과를 보관하는 객체에 대한 정의
    [Serializable]
    public class ImgSearch
    {
        /// 태그 목록에 대한 갯수
        public int Num_TagList { get; set; }
        /// 태그 목록 (NULL 허용)
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TagList> TagList { get; set; }
    }

    /// 태그 목록
    [HostProtectionAttribute(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
    public class TagList
    {
        /// 이미지 태그
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ImgTag { get; set; }
        /// 이미지 썸네일 URL
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ImgThumbURL { get; set; }
    }
}