﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Security.Permissions;

namespace MAM.Models
{
    /// <summary>
    /// 텍스트 검색 결과
    /// </summary>
    [Serializable]
    public class TextSearch
    {
        /// 텍스트 태그 명
        public string TxtTagName { get; set; }
        /// 질의 의도
        public string Interpretation { get; set; }
        /// 페이지 번호
        public int CurrentPageNo { get; set; }
        /// 전체 갯수
        public int ListCount { get; set; }
        /// 전체 페이지
        public int TotalPages { get; set; }
        /// 검색 결과
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Content> SearchResult { get; set; }
    }

    /// <summary>
    /// 검색 결과의 콘텐츠 정보
    /// </summary>
    [HostProtectionAttribute(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
    public class Content
    {
        /// 콘텐츠 이름
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string contentName { get; set; }
        /// 콘텐츠 아이디
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string contentID { get; set; }
        /// 카테고리 대
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string category1 { get; set; }
        /// 카테고리 중
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string category2 { get; set; }
        /// 카테고리 소
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string category3 { get; set; }
        /// 전화번호
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string telephone { get; set; }
        /// 주소
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address { get; set; }
        /// 홈페이지
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string homepage { get; set; }
        /// 상세 정보
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string overview { get; set; }
        /// 원본 이미지 URL
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OriginalImgURL { get; set; }
        /// 작은 이미지 URL
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SmallImgURL { get; set; }
        /// 카테고리 정보
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string categoryString { get; set; }

    }
}