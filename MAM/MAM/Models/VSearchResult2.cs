﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Security.Permissions;

namespace MAM.Models
{
    /// <summary>
    ///  비디오 검색 결과를 보여줄 때의 객체 정보
    /// </summary>
    public class VSearchResult2
    {
        /// 비디오 아이디
        public int VideoID { get; set; }
        /// 프레임 인덱스
        public int idx { get; set; }
        /// 제목
        public string title { get; set; }
        /// 상세정보
        public string overview { get; set; }
        /// 동영상 파일명
        public string filename { get; set; }
        /// RGB 차이
        public int RGBdifferce { get; set; }
        /// 이미지 파일명
        public string imgname { get; set; }
    }
}