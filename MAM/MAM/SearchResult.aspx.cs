﻿using System;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Web.Http;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Linq;
using System.Data;
using MAM.Models;
//using MySql.Data;
//using MySql.Data.MySqlClient;

namespace MAM
{
    public partial class SearchResult : System.Web.UI.Page
    {
        public List<Video> vdo = null;
        private List<VSearchResult> vSearchResults = null;
        public List<VSearchResult2> vSearchResults2 = null;

        public const int DefaultVideos = 5;
        public int vCnt = DefaultVideos;

        public string WA = null;
        public string Keywords = null;
        public string VideoSearch = null;
        public bool textSearch_noresult = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Form.Enctype = "multipart/form-data";

            WA = Request.QueryString["WA"];
            if (string.IsNullOrEmpty(WA)) WA = "off";
            string qstr = null;
            qstr = Request.QueryString["vCnt"]; 
            if (qstr != null && qstr != "") vCnt = Int32.Parse(qstr);

            if (!IsPostBack)
            {
                Keywords = Request.QueryString["keywords"]; 
                VideoSearch = Request.QueryString["VideoSearch"];//ㅇㅈ ~^~^~

                // 스마트폰에서 포스팅 되었을 때
                if (Request.UserAgent.Contains("Android"))
                {
                    if (Request.Cookies["VIDEOSEARCH"] != null && Request.Cookies["VIDEOSEARCH"].Value != "")
                    {
                        VideoSearch = Request.Cookies["VIDEOSEARCH"].Value;
                        MAM.Models.Utils.CookieManager("VIDEOSEARCH", "");
                    }
                }

                if (!String.IsNullOrEmpty(Keywords) && !Keywords.Contains("null")) SearchTextbox2.Text = Keywords;

                Debug.WriteLine("search text is " + SearchTextbox2.Text);
                Debug.Flush();

                try
                {
                    if (!string.IsNullOrEmpty(VideoSearch))
                    {

                     
                        string results = RetrieveDNAfromVideo(System.Web.HttpUtility.UrlDecode(VideoSearch)/*video name*/);

                        string[] lines = results.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        vSearchResults = new List<VSearchResult>();
                        foreach (string line in lines)
                        {
                            string[] words = line.Split(',');
                            VSearchResult vSearchResult = new VSearchResult();
                            vSearchResult.VideoID = Int32.Parse(words[0]);
                            vSearchResult.idx = Int32.Parse(words[1]);
                            vSearchResult.RGBdifferce = Int32.Parse(words[2]);
                            vSearchResults.Add(vSearchResult);
                        } 

                        StreamWriter file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                        file2.WriteLine("Video Search Start");
                        file2.Close();

                        MAMDataContext db = new MAMDataContext();
                      
                        List<int> VideoIDs = (List<int>)vSearchResults.Select(v => v.VideoID).ToList();
                      
                        vdo = (List<Video>)(from a in db.Video
                                            where VideoIDs.Contains(a.id)
                                            select a).ToList();  //ㅇㅈ vdo는 결국, RetrieveDNAfromVideo가 알려준유사동영상정보-> id가 같은동영상들
                      
                        vSearchResults2 = new List<VSearchResult2>();
                        VSearchResult v1 = null;

                        file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                        file2.WriteLine("Video Serach Middle");
                        file2.Close();

                        foreach (Video v in vdo)
                        {
                            VSearchResult2 v2 = new VSearchResult2();
                            v2.VideoID = v.id;
                            v2.overview = v.overview;
                            v2.title = v.title;
                            v2.filename = v.filename;
                            v2.imgname = v.imgname;
                            v1 = vSearchResults.Where(t => t.VideoID == v.id).OrderBy(t => t.RGBdifferce).FirstOrDefault();//ㅇㅈ ㅇㄱㅁㅇ
                           

                            v2.idx = v1.idx;
                            v2.RGBdifferce = v1.RGBdifferce;
                            vSearchResults2.Add(v2);
                        }
                        file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                        file2.WriteLine("Video Serach done");
                        file2.Close();
                        Debug.WriteLine("Video Serach done");
                        Debug.Flush();

                    }
                    if (!string.IsNullOrEmpty(Keywords))
                    {
                        //string ret2 = null;
                        string str1 = null;
                        if (string.IsNullOrEmpty(Keywords))
                        {
                            Keywords = SearchTextbox2.Text;
                        }
                        if (string.IsNullOrEmpty(str1)) str1 = Keywords; //ㅇㅈ str1 은 질의의도??
                        string[] searchTextArray = str1.Split(' ');
                        int cnt1 = searchTextArray.Count();
                        string st1 = ""; string st2 = ""; string st3 = "";
                        if (cnt1 > 0) st1 = searchTextArray[0];
                        if (cnt1 > 1) st2 = searchTextArray[1];
                        if (cnt1 > 2) st3 = searchTextArray[2];

                        MAMDataContext db = new MAMDataContext();
                        vdo = (List<Video>)db.Video.Where(v => v.overview.Contains(st1)
                                                || (cnt1 > 1 ? v.overview.Contains(st2) : false)
                                                || (cnt1 > 2 ? v.overview.Contains(st3) : false)).ToList();//ㅇㅈ 검색어를 overview에 가지고 있는 동영상 리스트

                        vSearchResults2 = new List<VSearchResult2>();
                        foreach (Video v in vdo)
                        {
                            VSearchResult2 v2 = new VSearchResult2();
                            v2.VideoID = v.id;
                            v2.overview = v.overview;
                            v2.title = v.title;
                            v2.filename = v.filename;
                            v2.idx = 0;
                            v2.RGBdifferce = 0;
                            v2.imgname = v.imgname;
                            vSearchResults2.Add(v2);
                        }
                        Debug.WriteLine("Video Search");
                        StreamWriter file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                        file2.WriteLine("Video Search");
                        file2.Close();
                    }

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Error in integration: " + ex.Message);
                    Debug.Flush();

                    StreamWriter file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                    file2.WriteLine("아님여기?"+ex.Message);
                    file2.Close(); //디버깅 계속
                }

                Debug.WriteLine("Search End");
            }
            if (fUpload.PostedFile != null)    
            {
                fileupload1();

            }
            else
            {
            }
        }

        protected void SearchCommand2_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("search text is " + SearchTextbox2.Text);
            Debug.Flush();

            if (!String.IsNullOrEmpty(SearchTextbox2.Text))
            {
                Response.Cookies.Remove("lastSearch");
                HttpCookie myCookie = new HttpCookie("lastSearch", SearchTextbox2.Text);
                myCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(myCookie);

                Response.Redirect("SearchResult.aspx?WA=" + WA + "&keywords=" + SearchTextbox2.Text);
            }
            Debug.WriteLine("Search End");
        }
        protected void SearchCommand2_Click(object sender, ImageClickEventArgs e) 
        {
            Debug.WriteLine("search text is " + SearchTextbox2.Text);
            Debug.Flush();

            if (!String.IsNullOrEmpty(SearchTextbox2.Text))
            {
                Response.Redirect("SearchResult.aspx?WA=" + WA + "&keywords=" + SearchTextbox2.Text);
            }
            Debug.WriteLine("Search End");
        }

        protected void btnUpload_Click(object sender, EventArgs e) 
        {
            // 참고: http://www.youtube.com/watch?v=dPUmQBowLfs //
            if (fUpload.HasFile)
            {
                fileupload1();
            }
            else
            {
                // No file selected...
            }
        }

        // 업로드 된 파일을 처리함.
        public void fileupload1() 
        {
            if ((fUpload.PostedFile.ContentType == "image/jpeg") ||
                (fUpload.PostedFile.ContentType == "image/png") ||
                (fUpload.PostedFile.ContentType == "image/bmp") ||
                (fUpload.PostedFile.ContentType == "image/gif"))
            {
                // 파일 사이즈 체크 등등
                string photoFolder = "C:\\Resources\\pictures\\";
                if (!Directory.Exists(photoFolder))
                    Directory.CreateDirectory(photoFolder);

                // 건대의 요청으로 문자열 형태의 파일명으로 전달하기로 함. 
                string Filename = "pic_" + MAM.Models.Utils.getUniqueID()
                                    + Path.GetExtension(fUpload.PostedFile.FileName);

                fUpload.SaveAs(photoFolder + Filename);
             

                Debug.WriteLine("Upload image is http://221.143.42.141:3000/pictures/" + Filename);
                Debug.Flush();
                Response.Redirect("SearchResult.aspx?WA=" + WA
                                    + "&ImageSearchTag=0&ImageSearch=http://221.143.42.141:3000/pictures/" + Filename);
                Debug.WriteLine("Search End");
            }
            else if ((fUpload.PostedFile.ContentType == "video/mp4") ||
                (fUpload.PostedFile.ContentType == "video/H264"))
            {
                // 파일 사이즈 체크 등등
                string videoFolder = "C:\\RTVS\\Resources\\VideosUploaded\\";
                if (!Directory.Exists(videoFolder))
                    Directory.CreateDirectory(videoFolder);

                // 업로드한 원래의 파일명을 보관하여 테스트시에 체크가 가능하도록 함.
                string Filename = "vid_" + DateTime.UtcNow.Ticks.ToString() + "."
                                    + Path.GetFileName(fUpload.PostedFile.FileName);

                fUpload.SaveAs(videoFolder + Filename);
               

                Debug.WriteLine("Upload video is http://221.143.42.141:3000/VideosUploaded/" + Filename);
                Debug.Flush();

               
                // 비디오 검색 결과로 이동
                Response.Redirect("SearchResult.aspx?WA=" + WA + "&sop=v"
                                    + "&VideoSearch=http://221.143.42.141:3000/VideosUploaded/" + Filename);
                Debug.WriteLine("VideoSearch Redirect");
            }
            else
            {
                // other file format의 처리
                Debug.WriteLine("알려지지 않은 파일 포맷: " + fUpload.PostedFile.ContentType + " "
                                    + fUpload.PostedFile.FileName);
              
                Debug.Flush();
            }
        }

        // DNA를 추출하여 유사 동영상을 돌려받음.
        protected string RetrieveDNAfromVideo(string videoName)
        {
            string strResult = "";
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 7788);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();

            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] buffer = encoder.GetBytes(videoName + "\r\n");

            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();

            int bytesRead;
            byte[] message = new byte[4096];

            string results = "";
            int fileLength = 0;

            try
            {
                while (true)
                {
                    bytesRead = 0;
                    try
                    {
                        //서버로부터 받을 때까지 대기
                        bytesRead = clientStream.Read(message, 0, 4096);
                    }
                    catch
                    {
                        //소켓에러시
                        break;
                    }
                    if (bytesRead == 0)
                    {
                        //데이터가 도착했지만 내용이 없음.
                        break;
                    }
                    //성공적으로 받음.
                

                    StreamWriter file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                    file2.WriteLine(encoder.GetString(message, 0, bytesRead));
                    file2.Close(); //디버깅 계속................
               
                    System.Diagnostics.Debug.WriteLine(encoder.GetString(message, 0, bytesRead));
                    //패킷이 분할되어서 도착시에 대비하여 EndString이 와야함. \r\n\r\n
                    results += encoder.GetString(message, 0, bytesRead);
                    if (!string.IsNullOrEmpty(results))
                    {
                        fileLength = results.IndexOf("\r\n\r\n"); // End String이 있으면 완료된 처리
                        if (fileLength != -1)
                        {
                            System.Diagnostics.Debug.WriteLine("response: " + results);
                            strResult = results.Substring(0, fileLength);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception: " + ex.Message);
                StreamWriter file2 = new StreamWriter("c:\\RTVS\\Log\\file.log", true);
                file2.WriteLine("여기라능"+ex.Message);
                file2.Close(); //디버깅 계속................
            }

            client.Close();

            return strResult;
        }

        protected void Logout_Click(Object sender, EventArgs e)
        {
            Response.Cookies["UserID"].Value = "";
            Response.Redirect(Request.RawUrl);
        }

        protected void SearchTextbox2_TextChanged(object sender, EventArgs e)
        {

        }


    }
}