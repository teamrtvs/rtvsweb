﻿//------------------------------------------------------------------------------
// <자동 생성됨>
//     이 코드는 도구를 사용하여 생성되었습니다.
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다. 
// </자동 생성됨>
//------------------------------------------------------------------------------

namespace MAM {
    
    
    public partial class SearchMain {
        
        /// <summary>
        /// form1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// MainLogo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image MainLogo;
        
        /// <summary>
        /// SearchTextbox 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchTextbox;
        
        /// <summary>
        /// SearchCommand 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton SearchCommand;
        
        /// <summary>
        /// fUpload 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload fUpload;
        
        /// <summary>
        /// btnUpload 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnUpload;
        
        /// <summary>
        /// VideoSearch 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하십시오.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton VideoSearch;
    }
}
