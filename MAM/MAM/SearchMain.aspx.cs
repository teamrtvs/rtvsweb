﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Net;
using MAM.Models;

namespace MAM
{
    public partial class SearchMain : System.Web.UI.Page
    {
        public string WA = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Form.Enctype = "multipart/form-data";
            WA = Request.QueryString["WA"];
            if (string.IsNullOrEmpty(WA)) WA = "off";


            if (fUpload.PostedFile != null)
            {
                fileupload1();

            }
        }

        protected void SearchCommand_Click(object sender, ImageClickEventArgs e)
        {
            Debug.WriteLine("search text is " + SearchTextbox.Text);
            Debug.Flush();

            if (SearchTextbox.Text != null && SearchTextbox.Text != "")
            {
                Response.Cookies.Remove("lastSearch");
                HttpCookie myCookie = new HttpCookie("lastSearch", SearchTextbox.Text);
                myCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(myCookie);

                Response.Redirect("SearchResult.aspx?wa="+WA+"&sop=a&ti=0&keywords=" + SearchTextbox.Text);
            }
            else
            {
                if (fUpload.PostedFile != null)
                {
                    fileupload1();

                }
            }

        
            Debug.WriteLine("Search End");
        }

        protected void ImageSearch_Click(object sender, ImageClickEventArgs e)
        {
    
            Response.Redirect("SearchResult.aspx?wa=" + WA + "&sop=i&ti=0&keywords=" + SearchTextbox.Text);
        }

        protected void TextSearch_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("SearchResult.aspx?wa=" + WA + "&sop=t&ti=0&keywords=" + SearchTextbox.Text);
        }

        protected void VideoSearch_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("SearchResult.aspx?wa=" + WA + "&sop=v&ti=0&keywords=" + SearchTextbox.Text);
        }

        protected void Logout_Click(Object sender, EventArgs e)
        {
            Response.Cookies["UserID"].Value = "";
            Response.Redirect(Request.RawUrl);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            // 참고: http://www.youtube.com/watch?v=dPUmQBowLfs
            if (fUpload.HasFile)
            {
                fileupload1();
            }
            else
            {
                // No file selected...
            }
        }

        public void fileupload1()
        {
            if ((fUpload.PostedFile.ContentType == "image/jpeg") ||
                (fUpload.PostedFile.ContentType == "image/png") ||
                (fUpload.PostedFile.ContentType == "image/bmp") ||
                (fUpload.PostedFile.ContentType == "image/gif"))
            {
                // 파일 사이즈 체크 등등
                string photoFolder = "d:\\Resources\\pictures\\";
                if (!Directory.Exists(photoFolder))
                    Directory.CreateDirectory(photoFolder);

                // 건대의 요청으로 문자열 형태의 파일명으로 전달하기로 함. 
                string Filename = "pic_" + MAM.Models.Utils.getUniqueID()
                                    + Path.GetExtension(fUpload.PostedFile.FileName);

                fUpload.SaveAs(photoFolder + Filename);
                //"http://221.143.42.141:8088/pictures/" + filename

                Debug.WriteLine("Upload image is http://221.143.42.141:8088/pictures/" + Filename);
                Debug.Flush();
                Response.Redirect("SearchResult.aspx?wa=" + WA + "&sop=a&ImageSearchTag=0&&ImageSearch=http://221.143.42.141:8088/pictures/" + Filename);
                Debug.WriteLine("Search End");
            }
            else if ((fUpload.PostedFile.ContentType == "video/mp4") ||
                        (fUpload.PostedFile.ContentType == "video/H264"))
            {
                // 파일 사이즈 체크 등등
                string videoFolder = "C:\\RTVS\\Resources\\VideosUploaded\\";
                if (!Directory.Exists(videoFolder))
                    Directory.CreateDirectory(videoFolder);

                // 업로드한 원래의 파일명을 보관하여 테스트시에 체크가 가능하도록 함.
                string Filename = "vid_" + DateTime.UtcNow.Ticks.ToString() + "."
                                    + Path.GetFileName(fUpload.PostedFile.FileName);

                fUpload.SaveAs(videoFolder + Filename);
                //"http://221.143.42.141:8088/VideosUploaded/" + filename

                Debug.WriteLine("Upload video is http://221.143.42.141:3000/VideosUploaded/" + Filename);
                Debug.Flush();

                // 비디오 검색 결과로 이동
                Response.Redirect("SearchResult.aspx?WA=" + WA + "&sop=v"
                                    + "&VideoSearch=http://221.143.42.141:3000/VideosUploaded/" + Filename);
                Debug.WriteLine("VideoSearch Redirect");
            }
            else
            {
                // other file format의 처리
            }
        }

    }
}